#!/usr/bin/env bash
read -p "Are you sure you want to lose all logs and delete the DB? (y/n):" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  rm ./splinter.db
  echo All cleared.
fi