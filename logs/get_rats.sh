#!/usr/bin/env bash
echo "What username are you searching for? (e.g. DOMAIN\\\\USER)"
read user
echo ""
cmd="SELECT DISTINCT timestamp, ratID, username FROM rats where username='$user'"
IFS=$'\n'
fqry=(`sqlite3 splinter.db "$cmd"`)
echo "Results:"
echo "------------------"

for f in "${fqry[@]}"; do
    echo "$f"
done